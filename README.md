# This Repo is Reserved for my Practice


#### NOTES 
```bash
1. ?
   - ?
```
#### Tracked Files
```
nothing tracked yet
```
#### You have an empty repository
```
To get started you will need to run these commands in your terminal.
```

#### Configure Git for the first time
```bash
git config --global user.name "Nenad Stevcic"
git config --global user.email "gism12a@gmail.com"
```
#### Working with your repository
```bash
```
#### I just want to clone this repository
```bash
If you want to simply clone this empty repository then run this command in your terminal.
git clone git clone https://stele1@bitbucket.org/stele1/test1.git
```
#### My code is ready to be pushed
```bash
If you already have code ready to be pushed to this repository then run this in your terminal.

cd existing-project
git init
git add --all
git commit -m "Initial Commit"
git remote add origin git clone https://stele1@bitbucket.org/stele1/test1.git
git push -u origin master
```
#### My code is already tracked by Git
```bash
If your code is already tracked by Git then set this repository as your "origin" to push to.

cd existing-project
git remote set-url origin git clone https://stele1@bitbucket.org/stele1/test1.git
git push -u origin --all
git push origin --tags
All done with the commands?
```

#### Authors
```
* **Nenad Stevcic** 
```
